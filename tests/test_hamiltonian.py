import dacite
import numpy as np

from nolopot.bound_states import hamiltonian
from nolopot.bound_states.potentials import DeltaShell, PotentialConfiguration


def test_2_point():
    parameters = {"type": "delta_shell", "variables": {"lambda_": -1024, "b": 10}}
    mass = 0.5
    potential_config = dacite.from_dict(
        data_class=PotentialConfiguration, data=parameters
    )
    potential = DeltaShell(potential_config, mass)

    correct_h = np.array(
        [[-62680.3871145464, -64709.1735217938], [-4645.90963040635, 20216.9558824076]]
    )

    h_matrix = hamiltonian.construct_h(2, 0.5, potential)[0]

    assert np.allclose(correct_h, h_matrix)
