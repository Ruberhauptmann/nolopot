import numpy as np

from nolopot.bound_states import gauss_quad


def test_gaussian_points_and_weights():
    correct_points = {"2": [-0.57735026919, 0.57735026919]}
    correct_weights = {"2": [1, 1]}

    for number_of_points in correct_points.keys():
        number_of_points = int(number_of_points)
        points, weights = gauss_quad.gaussian_quad(number_of_points, -1, 1)
        assert np.allclose(points, correct_points[str(number_of_points)])
        assert np.allclose(weights, correct_weights[str(number_of_points)])


def test_gaussian_points_and_weights_transform_0_200():
    correct_points = {"2": [100 - 100 / np.sqrt(3), 100 + 100 / np.sqrt(3)]}
    correct_weights = {"2": [100, 100]}

    for number_of_points in correct_points.keys():
        number_of_points = int(number_of_points)
        points, weights = gauss_quad.gaussian_quad(number_of_points, 0, 200)
        assert np.allclose(points, correct_points[str(number_of_points)])
        assert np.allclose(weights, correct_weights[str(number_of_points)])
