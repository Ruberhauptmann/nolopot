from pathlib import Path

import click

from nolopot.bound_states.commands import bound_states
from nolopot.logger import create_logger


@click.group()
@click.option("--quiet", is_flag=True, help="Will suppress terminal output.")
@click.option(
    "-logfile",
    "-l",
    type=click.Path(file_okay=True, path_type=Path),
    help="Print log to a logfile.",
)
def cli(quiet: bool, logfile: Path) -> None:
    create_logger(quiet, logfile)


cli.add_command(bound_states)
