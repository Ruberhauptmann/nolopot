import logging
import logging.config
from importlib.resources import as_file, files
from pathlib import Path

import yaml


def create_logger(quiet: bool, logfile: Path) -> logging.Logger:
    """Create a logger with logging options from package data.

    Returns:
        logging.Logger: Instance of Logger. Will be used anytime
            logging.getLogger() is called.
    """
    config_file = files("nolopot.data").joinpath("logging_base_config.yaml")
    with as_file(config_file) as config_file_path:
        with open(config_file_path) as f:
            config = yaml.safe_load(f)
    if quiet is True:
        config["handlers"].pop("console")
        config["loggers"]["nolopot"]["handlers"].remove("console")

    if logfile is None:
        config["handlers"].pop("file")
        config["loggers"]["nolopot"]["handlers"].remove("file")
    else:
        config["handlers"]["file"]["filename"] = logfile

    if quiet is True and logfile is None:
        config["loggers"]["nolopot"]["handlers"] = ["discard"]
    logging.config.dictConfig(config)
    logger = logging.getLogger(__name__)
    return logger
