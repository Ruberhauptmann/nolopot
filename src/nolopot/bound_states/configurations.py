from dataclasses import dataclass
from pathlib import Path

import nolopot.bound_states.potentials as potentials


@dataclass()
class BoundStateConfiguration:
    number_of_points: int
    potential: potentials.PotentialConfiguration
    mass: float
    result_file: Path
