import logging

import numpy as np
import numpy.typing as npt

from nolopot.bound_states.gauss_quad import gaussian_quad
from nolopot.bound_states.potentials import PotentialBase

logger = logging.getLogger(__name__)


def construct_h(
    number_of_points: int, mass: float, potential: PotentialBase
) -> tuple[npt.NDArray[np.float64], npt.NDArray[np.float64], npt.NDArray[np.float64]]:
    logger.info("Constructing Hamiltonian matrix with %s points.", number_of_points)
    k, w = gaussian_quad(number_of_points, lower_bound=0, upper_bound=200)

    hamilton = np.zeros((number_of_points, number_of_points))

    for i in range(0, number_of_points):
        for j in range(0, number_of_points):
            hamilton[i][j] = (
                2 / np.pi * potential.construct_potential(k[i], k[j]) * w[j] * k[j] ** 2
            )
            if i == j:
                hamilton[i][j] += k[j] ** 2 / (2 * mass)

    return hamilton, k, w
