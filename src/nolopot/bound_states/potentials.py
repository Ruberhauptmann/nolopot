"""Different potentials in k-space."""
import abc
import logging
from dataclasses import dataclass
from typing import Any

import numpy as np
import numpy.typing as npt
from scipy.special import lambertw

logger = logging.getLogger(__name__)


@dataclass
class PotentialConfiguration:
    type: str
    variables: dict[str, float]


class PotentialBase(metaclass=abc.ABCMeta):
    """Base class for potentials."""

    analytic_energy: float | None

    @property
    @abc.abstractmethod
    def type(self) -> str:
        pass

    @abc.abstractmethod
    def construct_potential(
        self,
        k: float | npt.NDArray[np.float64],
        k_prime: float | npt.NDArray[np.float64],
    ) -> float | npt.NDArray[np.float64]:
        pass


class DeltaShell(PotentialBase):
    """Implements the delta shell potential."""

    def __init__(self, config: PotentialConfiguration, mass: float) -> None:
        """Initialise an instance of the DeltaShell class."""
        logger.info("Initializing delta shell potential.")
        self.lambda_ = config.variables["lambda_"]
        self.mass = mass
        self.b = config.variables["b"]
        self.analytic_energy = np.real(
            -1
            / (2 * self.mass)
            * (
                -self.lambda_ / 2
                + 1
                / (2 * self.b)
                * lambertw(self.lambda_ * self.b * np.exp(self.lambda_ * self.b))
            )
            ** 2
        )

        logger.info(f"Analytic energy: {self.analytic_energy}")

    type = "delta_shell"

    def construct_potential(
        self,
        k: float | npt.NDArray[np.float64],
        k_prime: float | npt.NDArray[np.float64],
    ) -> float | npt.NDArray[np.float64] | Any:
        r"""Give the value of the potential for some :math:`k, k^{\\prime}`.

        Args:
            k (float | np.ndarray): First argument
            k_prime (float | np.ndarray): Second argument

        Returns:
            float | np.ndarray: value of potential.
        """
        return (
            self.lambda_
            / (2 * self.mass)
            * np.sin(k_prime * self.b)
            * np.sin(k * self.b)
            / (k * k_prime)
        )
