import logging
from pathlib import Path

import click

from nolopot.bound_states import read_file

logger = logging.getLogger(__name__)


@click.command()
@click.argument("filename", type=click.Path(exists=True, path_type=Path))
def bound_states(filename: Path) -> None:
    logger.info("Nonlocal potentials - Bound States")

    read_file(filename)
