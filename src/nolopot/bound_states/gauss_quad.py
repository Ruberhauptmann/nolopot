"""Calculate gaussian quadrature points."""
import logging
import sys
from typing import Literal

import numpy as np
import numpy.typing as npt


def gaussian_quad(
    number_of_points: int,
    lower_bound: float | Literal["-infty"],
    upper_bound: float | Literal["infty"],
) -> tuple[npt.NDArray[np.float64], npt.NDArray[np.float64]]:
    """Gaussian quadrature with general integration bounds.

    Args:
        number_of_points (int): Number of Gaussian integration points
        lower_bound (float | Literal['-infty']):
            Lower integration bound
        upper_bound (float | Literal['infty']):
            Upper integration bound

    Returns:
        tuple[np.ndarray, np.ndarray]: Two arrays with Gaussian points and
            weights
    """
    x_prime, w_prime = np.polynomial.legendre.leggauss(number_of_points)  # type: ignore

    if lower_bound == -1 and upper_bound == 1:
        x, w = x_prime, w_prime
    elif lower_bound == 0 and upper_bound == "infty":
        midpoint = 100
        x = midpoint * (1 + x_prime) / (1 - x_prime)
        w = 2 * midpoint * w_prime / (1 - x_prime) ** 2
    elif isinstance(lower_bound, (int, float)) and isinstance(
        upper_bound, (int, float)
    ):
        x = (lower_bound + upper_bound) / 2 + (upper_bound - lower_bound) / 2 * x_prime
        w = (upper_bound - lower_bound) / 2 * w_prime
    else:
        logging.error("Bounds not yet implemented.")
        sys.exit()

    return x, w
