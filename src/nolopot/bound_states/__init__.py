import logging
from pathlib import Path

import dacite
import h5py
import yaml

import nolopot.bound_states.potentials as potentials
from nolopot.bound_states import eigencalc
from nolopot.bound_states.configurations import BoundStateConfiguration
from nolopot.bound_states.result_class import BoundStateResult

logger = logging.getLogger(__name__)


def main(config: BoundStateConfiguration) -> None:
    if config.potential.type == "delta_shell":
        potential = potentials.DeltaShell(config.potential, config.mass)
    else:
        msg = f"Potential {config.potential.type} not implemented."
        logger.error(msg)
        raise RuntimeError(msg)

    results = eigencalc.eigensolver(config, potential)
    write_results(config.result_file, results)

    logger.info(
        "Bound state energy: %s +/- %s",
        results.energy,
        results.energy_absolute_precision,
    )
    logger.info("Relative precision: %s", results.energy_relative_precision)


def read_file(filename: Path) -> None:
    with open(filename, "r") as file:
        logger.info("Initializing configuration with:")
        config_dict = yaml.safe_load(file)
        config_dict["result_file"] = Path(config_dict["result_file"])
        if config_dict["result_file"] != ".hdf5":
            logger.warning("Extension for result file is not .hdf5.")
        for key in config_dict.keys():
            logger.info("%s: %s", key, config_dict[key])
        config = dacite.from_dict(data_class=BoundStateConfiguration, data=config_dict)

    main(config)


def write_results(filename: Path, results: BoundStateResult) -> None:
    with h5py.File(filename, "w") as f:
        f.attrs["energy"] = results.energy
        f.attrs["energy_precision"] = results.energy_absolute_precision
        if results.analytic_energy:
            f.attrs["analytic_energy"] = results.analytic_energy
        f.attrs["potential"] = results.config.potential.type
        f.attrs["mass"] = results.config.mass
        for key, value in results.config.potential.variables.items():
            f.attrs[key] = value

        k_points = f.create_dataset(name="k points", data=results.k_points)
        k_points.make_scale("1/A")

        wavefunctions_k_space = f.create_dataset(
            name="wavefunction k space", data=results.wavefunction_k_space
        )
        wavefunctions_k_space.dims[0].label = "k points"
        wavefunctions_k_space.dims[0].attach_scale(k_points)

        f.create_dataset(name="gaussian weights", data=results.weights)
