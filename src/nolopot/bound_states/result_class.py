from dataclasses import dataclass

import numpy as np
import numpy.typing as npt

from nolopot.bound_states.configurations import BoundStateConfiguration


@dataclass
class BoundStateResult:
    config: BoundStateConfiguration
    energy: float
    analytic_energy: float | None
    energy_relative_precision: float
    energy_absolute_precision: float
    wavefunction_k_space: npt.NDArray[np.float64]
    k_points: npt.NDArray[np.float64]
    weights: npt.NDArray[np.float64]
