"""Solves the eigenvalue problem, searches for bound states."""
import logging

import numpy as np
import numpy.typing as npt
from scipy import linalg

from nolopot.bound_states.configurations import BoundStateConfiguration
from nolopot.bound_states.hamiltonian import construct_h
from nolopot.bound_states.potentials import PotentialBase
from nolopot.bound_states.result_class import BoundStateResult

logger = logging.getLogger(__name__)


def eigensolver(
    config: BoundStateConfiguration, potential: PotentialBase
) -> BoundStateResult:
    test_points = list(range(config.number_of_points - 16, config.number_of_points, 8))
    logger.info(
        "Will also make runs with %s points to estimate precision "
        "of the found energy.",
        test_points,
    )

    hamiltonian, k_points, weights = construct_h(
        config.number_of_points, config.mass, potential
    )
    eigenvalues, eigenvectors = linalg.eig(hamiltonian)
    if not np.all(eigenvalues.imag == 0):
        logger.info(
            "Eigenvalues have imaginary parts, this means the hamiltonian "
            "matrix is not constructed correctly."
        )
    eigenvalues = eigenvalues.real
    eigenvectors = eigenvectors.T
    possible_energies: dict[int, float] = {}
    other_eigenvalues: list[float] = []

    for number_of_points in test_points:
        hamiltonian_compare = construct_h(number_of_points, config.mass, potential)[0]
        eigenvalues_compare, _ = linalg.eig(hamiltonian_compare)
        eigenvalues_compare = eigenvalues_compare.real
        for i, ev in enumerate(eigenvalues[eigenvalues < 0]):
            for compare_ev in eigenvalues_compare[eigenvalues_compare < 0]:
                if abs((compare_ev - ev) / (compare_ev + ev)) < 0.1:
                    other_eigenvalues.append(compare_ev)
                    possible_energies[i] = ev

    if np.all(possible_energies[0] == np.array(list(possible_energies.values()))):
        logger.info("Found one bound state.")
        energy = possible_energies[0]
        precision = np.sqrt(
            1
            / len(other_eigenvalues)
            * np.sum((energy - np.array(other_eigenvalues)) ** 2)
        )
        precision_relative = precision / energy
        wavefunction = eigenvectors[next(iter(possible_energies))]
    else:
        msg = "Energy changing too much with number of grid points"
        logger.error(msg)
        raise RuntimeError(msg)

    _verify_eigenvector(hamiltonian, wavefunction)

    result = BoundStateResult(
        config=config,
        energy=energy,
        energy_absolute_precision=precision,
        analytic_energy=potential.analytic_energy,
        energy_relative_precision=precision_relative,
        wavefunction_k_space=wavefunction,
        k_points=k_points,
        weights=weights,
    )

    return result


def _verify_eigenvector(
    h: npt.NDArray[np.float64], ev: npt.NDArray[np.float64]
) -> None:
    """Test that a given ev is indeed an eigenvector of the matrix h.

    Args:
        h (np.ndarray): matrix to test
        ev (np.ndarray): eigenvector to test

    Raises:
        RuntimeError: raised if ev is not an eigenvector of h
    """
    test_product = np.matmul(h, ev) / ev
    if not np.allclose(test_product, np.mean(test_product)):
        msg = "Eigenvector error"
        logger.error(msg)
        raise RuntimeError(msg)
