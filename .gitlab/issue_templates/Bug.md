<!-- You can use this template to give notice about a bug in the software. -->

### Summary

<!-- Summarize the bug encountered concisely. -->

### Steps to reproduce

<!-- Describe how one can reproduce the issue - this is very important. Please use an ordered list. -->

### What is the current *bug* behavior?

<!-- Describe what actually happens. -->

### What is the expected *correct* behavior?

<!-- Describe what you should see instead. -->

### Environment and logs

<!-- At least, you should provide the version of nolopot you are using, as well as the input file/a link to the input file.

It's also useful to provide information about hardware and the operating system. -->

### Relevant logs and hardware information

<!-- Paste any relevant logs - please use code blocks (```) to format console output, logs, and code as it's tough to read otherwise. -->

### Possible fixes

<!-- If you can, link to the line of code that might be responsible for the problem. -->

/label ~bug
