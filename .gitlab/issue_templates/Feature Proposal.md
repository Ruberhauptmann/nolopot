<!-- You can use this template to propose a feature.

At first, it will be labelled as a ~suggestion, it will get the ~upcoming label once its decided it is a feature to be worked on.

Also, the ~documentation label will be applied, it will be removed once sufficient documentation for the feature is provided. -->

### Problem to solve

<!-- Give a description of the problem you are trying to solve with this issue. -->

### Proposal

<!-- Use this section to explain the feature in detail and how it will work.
It can be helpful to add technical details, design proposals, and links to related epics or issues. -->


/label ~feature ~suggestion ~documentation
