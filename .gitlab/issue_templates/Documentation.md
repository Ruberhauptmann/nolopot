<!-- You can use this template to propose parts of documentation that need to be updated in some way.

This can just be parts of the API documentation, or information on how to do certain higher-level things with the software. -->

### Problem to solve

<!-- Depending on the kind of problem, include the following details:
* What docs or doc section affected? Include links or paths.
* Is there a problem with a specific document, or a feature/process that's not addressed sufficiently in docs?
* Any other ideas or requests? -->

### Proposal

<!-- Further specifics for how the problem can be solved. -->

/label ~maintenance ~documentation
