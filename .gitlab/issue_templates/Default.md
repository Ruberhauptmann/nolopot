Depending on the type of issue you want to raise, you can use one of the issue template to provide a bug report, suggestions for new features in the software or changes in the documentation.

If you feel like there is no template fitting for the type of issue you want to raise, just state your issue here.
