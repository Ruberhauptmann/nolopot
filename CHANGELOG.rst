
.. _changelog-0.0.3:

0.0.3 — 2024-02-03
==================

Added
-----

- Build a docker image on release

.. _changelog-0.0.2:

0.0.2 — 2024-02-01
==================

Added
-----

- Description templates for issues and merge requests

- Save results into hdf5 file

.. _changelog-0.0.1:

0.0.1 - 2023-09-28
===================

Added
-----

- Starting project layout
