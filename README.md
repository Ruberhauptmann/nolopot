# nolopot

Python package to calculate bound states and (someday) scattering for non-local potentials

[![coverage report](https://gitlab.com/Ruberhauptmann/nolopot/badges/main/coverage.svg)](https://gitlab.com/Ruberhauptmann/nolopot/-/commits/main)
[![pipeline status](https://gitlab.com/Ruberhauptmann/nolopot/badges/main/pipeline.svg)](https://gitlab.com/Ruberhauptmann/nolopot/-/commits/main)
[![Latest Release](https://gitlab.com/Ruberhauptmann/nolopot/-/badges/release.svg)](https://gitlab.com/Ruberhauptmann/nolopot/-/releases)

* Documentation: https://nolopot.readthedocs.io

## Installation

The package can be installed via

`pip install nolopot --no-deps --index-url https://gitlab.com/api/v4/projects/50799449/packages/pypi/simple

## Usage

## Contributing

You are welcome to open an issue if you want something changed or added in the software or if there are bugs occuring.
There are templates available to handle several types of possible issues, feel encouraged to use them to specify details and get the metadata added automatically.

### Developing

You can also help develop this software further.
This should help you get set up to start this.

Prerequisites:
* make
* python
* conda

Set up the development environment:
* clone the repository
* run `make environment`
* now activate the conda environment `conda activate nolopot-dev`

Now you can create a separate branch to work on the project.

You can manually run tests using for example `tox -e py310` (for running against python 3.10).
After pushing your branch, all tests will also be run via Gitlab CI.
Whenever adding a new

Using `pre-commit`, automatic linting and formatting is done before every commit, which may cause the first commit to fail.
A second try should then succeed.

After you are done working on an issue and all tests are running successful, you can add a new piece of changelog via `scriv create` and make a merge request.
