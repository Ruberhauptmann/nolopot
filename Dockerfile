FROM python:3.10.13-slim

ARG NOLOPOT_VERSION=latest

WORKDIR /usr/src/nolopot/

RUN pip install nolopot==$NOLOPOT_VERSION --index-url https://gitlab.com/api/v4/projects/50799449/packages/pypi/simple
