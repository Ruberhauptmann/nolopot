.. nolopot documentation master file, created by
   sphinx-quickstart on Fri Feb 10 08:06:21 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to nolopot's documentation!
===================================

.. toctree::
   :caption: Contents:
   :maxdepth: 4

   release
   api



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
