Release
=======

.. contents:: Content

Main branch represents the current development version of the package.

Create a release:

- Create a release branch (called ``release-vx.x.x``)
- Run all tests
- Build changelog (``scriv collect --version vx.x.x``)
- Commit everything changed
- Push to release branch
- Merge release branch into main
- Set a git tag with changelog content
